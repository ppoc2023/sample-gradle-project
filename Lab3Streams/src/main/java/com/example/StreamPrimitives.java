package com.example;

import it.unimi.dsi.fastutil.doubles.DoubleArrayList;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamPrimitives {
    public Optional<Double> sum(DoubleArrayList doubles){
        return doubles.stream()
                .reduce(Double::sum);
    }

    public Optional<Double> average(DoubleArrayList doubles){
        return doubles.stream()
                .reduce(Double::sum)
                .map(
                        t -> {
                            return t/doubles.size();
                        }
                );
    }

    public List<Double> top10percent(DoubleArrayList doubles){
        return doubles.stream()
                .sorted((d1, d2) -> {
                    if(d1 > d2){
                        return -1;
                    } else if (d1 < d2) {
                        return 1;
                    }
                    return 0;
                })
                .limit(doubles.size()/10)
                .collect(Collectors.toList());
    }
}
