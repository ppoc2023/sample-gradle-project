package com.example;

import java.math.BigInteger;
import java.util.Collection;
import java.util.stream.Collectors;

public class StreamObjects<T extends Number> {
    public T sum(Collection<T> elements){
        return elements.stream()
                .reduce((T t1, T t2) -> {
                    if (t1 instanceof BigInteger && t2 instanceof BigInteger) {
                        return (T) BigInteger.valueOf(t1.intValue() + t2.intValue());
                    } else if (t1 instanceof Double && t2 instanceof Double) {
                        return (T) Double.valueOf(t1.doubleValue() + t2.doubleValue());
                    } else {
                        throw new IllegalArgumentException("Unsupported type");
                    }
                })
                .orElse(null);
    }

    public T average(Collection<T> elements){
        return elements.stream()
                .reduce((T t1, T t2) -> {
                    if (t1 instanceof BigInteger && t2 instanceof BigInteger) {
                        return (T) BigInteger.valueOf(t1.intValue() + t2.intValue());
                    } else if (t1 instanceof Double && t2 instanceof Double) {
                        return (T) Double.valueOf(t1.doubleValue() + t2.doubleValue());
                    } else {
                        throw new IllegalArgumentException("Unsupported type");
                    }
                })
                .map(t -> {
                    if (t instanceof BigInteger) {
                        return (T) BigInteger.valueOf(t.intValue()/ elements.size());
                    } else if (t instanceof Double) {
                        return (T) Double.valueOf(t.doubleValue()/elements.size());
                    } else {
                        throw new IllegalArgumentException("Unsupported number type");
                    }
                })
                .orElse(null);
    }

    public Collection<T> top10percent(Collection<T> elements){
        return elements.stream()
                .sorted((o1, o2) -> {
                    if (o1 instanceof BigInteger && o2 instanceof BigInteger){
                        if(o1.intValue() > o2.intValue()){
                            return -1;
                        } else if (o1.intValue() < o2.intValue()) {
                            return 1;
                        }
                        return 0;
                    } else if (o1 instanceof Double && o2 instanceof Double) {
                        if(o1.doubleValue() > o2.doubleValue()){
                            return -1;
                        } else if (o1.intValue() < o2.intValue()) {
                            return 1;
                        }
                        return 0;
                    }
                    return 0;
                })
                .limit(elements.size()/10)
                .collect(Collectors.toList());
    }
}
