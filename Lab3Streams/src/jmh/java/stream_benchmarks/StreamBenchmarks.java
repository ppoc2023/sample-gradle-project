package stream_benchmarks;

import com.example.StreamObjects;
import com.example.StreamPrimitives;
import it.unimi.dsi.fastutil.doubles.DoubleArrayList;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;

import java.util.List;

public class StreamBenchmarks {
    @State(Scope.Benchmark)
    public static class DoubleAscendingElementsExecutionPlan {
        public List<Double> doubleList = new ArrayList<>();
        public StreamObjects<Double> doubleStreamObjects = new StreamObjects<>();

        @Setup(Level.Invocation)
        public void setUp(){
            doubleList.clear();
            for(int i = 1; i <= 100000000; i++){
                doubleList.add((double) i);
            }
        }
    }

    @State(Scope.Benchmark)
    public static class DoubleDescendingElementsExecutionPlan {
        public List<Double> doubleList = new ArrayList<>();
        public StreamObjects<Double> doubleStreamObjects = new StreamObjects<>();

        @Setup(Level.Invocation)
        public void setUp(){
            doubleList.clear();
            for(int i = 100000000; i >= 1; i--){
                doubleList.add((double) i);
            }
        }
    }

    @State(Scope.Benchmark)
    public static class DoubleRandomElementsExecutionPlan {
        public List<Double> doubleList = new ArrayList<>();
        public StreamObjects<Double> doubleStreamObjects = new StreamObjects<>();

        @Setup(Level.Invocation)
        public void setUp(){
            Random random = new Random();
            doubleList.clear();
            for(int i = 1; i <= 100000000; i++){
                doubleList.add(random.nextDouble(100000000- 1) + 1);
            }
        }
    }

    @State(Scope.Benchmark)
    public static class PrimitiveDoubleAscendingElementsExecutionPlan {
        public DoubleArrayList doubleArrayList = new DoubleArrayList();
        public StreamPrimitives streamPrimitives = new StreamPrimitives();

        @Setup(Level.Invocation)
        public void setUp(){
            doubleArrayList.clear();
            for(int i = 1; i <= 100000000; i++){
                doubleArrayList.add(i);
            }
        }
    }

    @State(Scope.Benchmark)
    public static class PrimitiveDoubleDescendingElementsExecutionPlan {
        public DoubleArrayList doubleArrayList = new DoubleArrayList();
        public StreamPrimitives streamPrimitives = new StreamPrimitives();

        @Setup(Level.Invocation)
        public void setUp(){
            doubleArrayList.clear();
            for(int i = 100000000; i >= 1; i--){
                doubleArrayList.add(i);
            }
        }
    }

    @State(Scope.Benchmark)
    public static class PrimitiveDoubleRandomElementsExecutionPlan {
        public DoubleArrayList doubleArrayList = new DoubleArrayList();
        public StreamPrimitives streamPrimitives = new StreamPrimitives();

        @Setup(Level.Invocation)
        public void setUp(){
            Random random = new Random();
            doubleArrayList.clear();
            for(int i = 1; i <= 100000000; i++){
                doubleArrayList.add(random.nextDouble(100000000- 1) + 1);
            }
        }
    }

    @Benchmark
    public void DoubleAscendingElementsSumBenchmark(DoubleAscendingElementsExecutionPlan doubleAscendingElementsExecutionPlan, Blackhole consumer){
        Double sum = doubleAscendingElementsExecutionPlan.doubleStreamObjects.sum(doubleAscendingElementsExecutionPlan.doubleList);
        consumer.consume(sum);
    }

    @Benchmark
    public void DoubleAscendingElementsAvgBenchmark(DoubleAscendingElementsExecutionPlan doubleAscendingElementsExecutionPlan, Blackhole consumer){
        Double avg = doubleAscendingElementsExecutionPlan.doubleStreamObjects.average(doubleAscendingElementsExecutionPlan.doubleList);
        consumer.consume(avg);
    }

    @Benchmark
    public void DoubleAscendingElementsTop10Benchmark(DoubleAscendingElementsExecutionPlan doubleAscendingElementsExecutionPlan, Blackhole consumer){
        List<Double> doubles = (List<Double>) doubleAscendingElementsExecutionPlan.doubleStreamObjects.top10percent(doubleAscendingElementsExecutionPlan.doubleList);
        consumer.consume(doubles);
    }

    @Benchmark
    public void DoubleDescendingElementsSumBenchmark(DoubleDescendingElementsExecutionPlan doubleDescendingElementsExecutionPlan, Blackhole consumer){
        Double sum = doubleDescendingElementsExecutionPlan.doubleStreamObjects.sum(doubleDescendingElementsExecutionPlan.doubleList);
        consumer.consume(sum);
    }

    @Benchmark
    public void DoubleDescendingElementsAvgBenchmark(DoubleDescendingElementsExecutionPlan doubleDescendingElementsExecutionPlan, Blackhole consumer){
        Double avg = doubleDescendingElementsExecutionPlan.doubleStreamObjects.average(doubleDescendingElementsExecutionPlan.doubleList);
        consumer.consume(avg);
    }

    @Benchmark
    public void DoubleDescendingElementsTop10Benchmark(DoubleDescendingElementsExecutionPlan doubleDescendingElementsExecutionPlan, Blackhole consumer){
        List<Double> doubles = (List<Double>) doubleDescendingElementsExecutionPlan.doubleStreamObjects.top10percent(doubleDescendingElementsExecutionPlan.doubleList);
        consumer.consume(doubles);
    }

    @Benchmark
    public void DoubleRandomElementsSumBenchmark(DoubleRandomElementsExecutionPlan doubleRandomElementsExecutionPlan, Blackhole consumer){
        Double sum = doubleRandomElementsExecutionPlan.doubleStreamObjects.sum(doubleRandomElementsExecutionPlan.doubleList);
        consumer.consume(sum);
    }

    @Benchmark
    public void DoubleRandomElementsAvgBenchmark(DoubleRandomElementsExecutionPlan doubleRandomElementsExecutionPlan, Blackhole consumer){
        Double avg = doubleRandomElementsExecutionPlan.doubleStreamObjects.average(doubleRandomElementsExecutionPlan.doubleList);
        consumer.consume(avg);
    }

    @Benchmark
    public void DoubleRandomElementsTop10Benchmark(DoubleRandomElementsExecutionPlan doubleRandomElementsExecutionPlan, Blackhole consumer){
        List<Double> doubles = (List<Double>) doubleRandomElementsExecutionPlan.doubleStreamObjects.top10percent(doubleRandomElementsExecutionPlan.doubleList);
        consumer.consume(doubles);
    }

    @Benchmark
    public void PrimitiveDoubleAscendingElementsSumBenchmark(PrimitiveDoubleAscendingElementsExecutionPlan primitiveDoubleAscendingElementsExecutionPlan, Blackhole consumer){
        Optional<Double> sum = primitiveDoubleAscendingElementsExecutionPlan.streamPrimitives.sum(primitiveDoubleAscendingElementsExecutionPlan.doubleArrayList);
        consumer.consume(sum);
    }

    @Benchmark
    public void PrimitiveDoubleAscendingElementsAvgBenchmark(PrimitiveDoubleAscendingElementsExecutionPlan primitiveDoubleAscendingElementsExecutionPlan, Blackhole consumer){
        Optional<Double> avg = primitiveDoubleAscendingElementsExecutionPlan.streamPrimitives.average(primitiveDoubleAscendingElementsExecutionPlan.doubleArrayList);
        consumer.consume(avg);
    }

    @Benchmark
    public void PrimitiveDoubleAscendingElementsTop10Benchmark(PrimitiveDoubleAscendingElementsExecutionPlan primitiveDoubleAscendingElementsExecutionPlan, Blackhole consumer){
        List<Double> doubles = primitiveDoubleAscendingElementsExecutionPlan.streamPrimitives.top10percent(primitiveDoubleAscendingElementsExecutionPlan.doubleArrayList);
        consumer.consume(doubles);
    }

    @Benchmark
    public void PrimitiveDoubleDescendingElementsSumBenchmark(PrimitiveDoubleDescendingElementsExecutionPlan primitiveDoubleDescendingElementsExecutionPlan, Blackhole consumer){
        Optional<Double> sum = primitiveDoubleDescendingElementsExecutionPlan.streamPrimitives.sum(primitiveDoubleDescendingElementsExecutionPlan.doubleArrayList);
        consumer.consume(sum);
    }

    @Benchmark
    public void PrimitiveDoubleDescendingElementsAvgBenchmark(PrimitiveDoubleDescendingElementsExecutionPlan primitiveDoubleDescendingElementsExecutionPlan, Blackhole consumer){
        Optional<Double> avg = primitiveDoubleDescendingElementsExecutionPlan.streamPrimitives.average(primitiveDoubleDescendingElementsExecutionPlan.doubleArrayList);
        consumer.consume(avg);
    }

    @Benchmark
    public void PrimitiveDoubleDescendingElementsTop10Benchmark(PrimitiveDoubleDescendingElementsExecutionPlan primitiveDoubleDescendingElementsExecutionPlan, Blackhole consumer){
        List<Double> doubles = primitiveDoubleDescendingElementsExecutionPlan.streamPrimitives.top10percent(primitiveDoubleDescendingElementsExecutionPlan.doubleArrayList);
        consumer.consume(doubles);
    }

    @Benchmark
    public void PrimitiveDoubleRandomElementsSumBenchmark(PrimitiveDoubleRandomElementsExecutionPlan primitiveDoubleRandomElementsExecutionPlan, Blackhole consumer){
        Optional<Double> sum = primitiveDoubleRandomElementsExecutionPlan.streamPrimitives.sum(primitiveDoubleRandomElementsExecutionPlan.doubleArrayList);
        consumer.consume(sum);
    }

    @Benchmark
    public void PrimitiveDoubleRandomElementsAvgBenchmark(PrimitiveDoubleRandomElementsExecutionPlan primitiveDoubleRandomElementsExecutionPlan, Blackhole consumer){
        Optional<Double> avg = primitiveDoubleRandomElementsExecutionPlan.streamPrimitives.average(primitiveDoubleRandomElementsExecutionPlan.doubleArrayList);
        consumer.consume(avg);
    }

    @Benchmark
    public void PrimitiveDoubleRandomElementsTop10Benchmark(PrimitiveDoubleRandomElementsExecutionPlan primitiveDoubleRandomElementsExecutionPlan, Blackhole consumer){
        List<Double> doubles = primitiveDoubleRandomElementsExecutionPlan.streamPrimitives.top10percent(primitiveDoubleRandomElementsExecutionPlan.doubleArrayList);
        consumer.consume(doubles);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(StreamBenchmarks.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();
    }
}