import com.example.StreamObjects;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.is;

public class TestStreamObjects {
    public StreamObjects<BigInteger> bigIntegerStreamObjects;
    public StreamObjects<Double> doubleStreamObjects;
    public List<BigInteger> bigIntegerList;
    public List<Double> doubleList;

    @BeforeEach
    public void setup(){
        this.bigIntegerStreamObjects = new StreamObjects<>();
        this.doubleStreamObjects = new StreamObjects<>();
        this.bigIntegerList = new ArrayList<>();
        this.doubleList = new ArrayList<>();
    }

    @Test
    public void testBigIntegerSum(){
        //arrange
        for(int i = 0; i < 100; i++){
            this.bigIntegerList.add(BigInteger.valueOf(i));
        }

        //act
        BigInteger sum = bigIntegerStreamObjects.sum(this.bigIntegerList);

        //assert
        MatcherAssert.assertThat(sum, is(BigInteger.valueOf(4950)));
    }

    @Test
    public void testDoubleSum(){
        for(int i = 0; i < 100; i++){
            this.doubleList.add((double) i);
        }

        Double sum = doubleStreamObjects.sum(this.doubleList);

        MatcherAssert.assertThat(sum, is((double) 4950));
    }

    @Test
    public void testBigIntegerAverage(){
        for(int i = 0; i < 100; i++){
            this.bigIntegerList.add(BigInteger.valueOf(i));
        }

        BigInteger avg = bigIntegerStreamObjects.average(this.bigIntegerList);

        MatcherAssert.assertThat(avg, is(BigInteger.valueOf(49)));
    }

    @Test
    public void testDoubleAverage(){
        for(int i = 0; i < 100; i++){
            this.doubleList.add((double) i);
        }

        Double avg = doubleStreamObjects.average(this.doubleList);

        MatcherAssert.assertThat(avg, is(49.5));
    }

    @Test
    public void testTop10PercentBigInteger(){
        for(int i = 0; i < 100; i++){
            this.bigIntegerList.add(BigInteger.valueOf(i));
        }

        List<BigInteger> top10List = (List<BigInteger>) bigIntegerStreamObjects.top10percent(this.bigIntegerList);

        BigInteger val = BigInteger.valueOf(99);
        for(BigInteger element: top10List){
            MatcherAssert.assertThat(element, is(val));
            val = val.subtract(BigInteger.valueOf(1));
        }
    }

    @Test
    public void testTop10PercentDouble(){
        for(int i = 0; i < 100; i++){
            this.doubleList.add((double) i);
        }

        List<Double> top10List = (List<Double>) doubleStreamObjects.top10percent(this.doubleList);

        double val = 99;
        for(Double element: top10List){
            MatcherAssert.assertThat(element, is(val));
            val -= 1;
        }
    }
}
