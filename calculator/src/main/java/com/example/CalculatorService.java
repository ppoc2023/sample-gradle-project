package com.example;

public class CalculatorService implements ICalculatorService{
    private final ICalculator calculator;

    public CalculatorService(BasicCalculator calculator){
        this.calculator = calculator;
    }

    @Override
    public void clearCurrent_value() {
        this.calculator.clearCurrent_value();
    }

    @Override
    public void addVal(int val) {
        this.calculator.addVal(val);
    }

    @Override
    public void subtractVal(int val) {
        this.calculator.subtractVal(val);
    }

    @Override
    public void maxVal(int val) {
        this.calculator.maxVal(val);
    }

    @Override
    public void minVal(int val) {
        this.calculator.minVal(val);
    }

    @Override
    public void divideVal(int val) throws Exception {
        this.calculator.divideVal(val);
    }

    @Override
    public void sqrtVal() {
        this.calculator.sqrtVal();
    }

    @Override
    public void multiplyVal(int val) {
        this.calculator.multiplyVal(val);
    }

    @Override
    public double getCurrent_value() {
        return this.calculator.getCurrent_value();
    }
}
