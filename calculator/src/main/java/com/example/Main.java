package com.example;


public class Main {
    public static void main(String[] args) {
        BasicCalculator basicCalculator = new BasicCalculator();
        CalculatorService calculatorService = new CalculatorService(basicCalculator);
        CLIHandler CLIHandler = new CLIHandler(calculatorService);

        CLIHandler.printWelcomeMessage();
        CLIHandler.executeProgram();
    }
}