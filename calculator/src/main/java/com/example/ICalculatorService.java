package com.example;

public interface ICalculatorService {
    void clearCurrent_value();

    void addVal(int val);

    void subtractVal(int val);

    void maxVal(int val);

    void minVal(int val);

    void divideVal(int val) throws Exception;

    void sqrtVal();

    void multiplyVal(int val);

    double getCurrent_value();
}
