package com.example;

import java.util.Objects;
import java.util.Scanner;

public class CLIHandler {
    private final Scanner scanner;

    private final ICalculatorService calculatorService;

    public CLIHandler(CalculatorService calculatorService){
        scanner = new Scanner(System.in);
        this.calculatorService = calculatorService;
    }

    void printWelcomeMessage(){
        System.out.println("~Extremely Advanced Calculator v0.0.1~\n\n " +
                "The supported operations are +, -, /, *, min, max, sqrt and AC. To close the program type exit.\n");
        System.out.println("Current value: " + calculatorService.getCurrent_value() + "\n");
        System.out.println("Enter the desired operation: ");
    }

    void executeProgram(){
        String operation = scanner.nextLine();
        int operation_value;

        while(!Objects.equals(operation, "exit")){
            try {
                if (Objects.equals(operation, "+")) {
                    System.out.println("Enter a value:");
                    if(scanner.hasNextInt()){
                        operation_value = scanner.nextInt();
                        calculatorService.addVal(operation_value);
                        scanner.nextLine();
                    }
                    else{
                        System.out.println("Not a valid input!");
                        scanner.nextLine();
                    }
                } else if (Objects.equals(operation, "-")) {
                    System.out.println("Enter a value:");
                    if(scanner.hasNextInt()){
                        operation_value = scanner.nextInt();
                        calculatorService.subtractVal(operation_value);
                        scanner.nextLine();
                    }
                    else{
                        System.out.println("Not a valid input!");
                        scanner.nextLine();
                    }
                } else if (Objects.equals(operation, "/")) {
                    System.out.println("Enter a value:");
                    if(scanner.hasNextInt()){
                        operation_value = scanner.nextInt();
                        calculatorService.divideVal(operation_value);
                        scanner.nextLine();
                    }
                    else{
                        System.out.println("Not a valid input!");
                        scanner.nextLine();
                    }
                } else if (Objects.equals(operation, "*")) {
                    System.out.println("Enter a value:");
                    if(scanner.hasNextInt()){
                        operation_value = scanner.nextInt();
                        calculatorService.multiplyVal(operation_value);
                        scanner.nextLine();
                    }
                    else{
                        System.out.println("Not a valid input!");
                        scanner.nextLine();
                    }
                } else if (Objects.equals(operation, "sqrt")) {
                    calculatorService.sqrtVal();
                } else if (Objects.equals(operation, "max")) {
                    System.out.println("Enter a value:");
                    if(scanner.hasNextInt()){
                        operation_value = scanner.nextInt();
                        calculatorService.maxVal(operation_value);
                        scanner.nextLine();
                    }
                    else{
                        System.out.println("Not a valid input!");
                        scanner.nextLine();
                    }
                } else if (Objects.equals(operation, "min")) {
                    System.out.println("Enter a value:");
                    if(scanner.hasNextInt()){
                        operation_value = scanner.nextInt();
                        calculatorService.minVal(operation_value);
                        scanner.nextLine();
                    }
                    else{
                        System.out.println("Not a valid input!");
                        scanner.nextLine();
                    }
                } else if (Objects.equals(operation, "AC")) {
                    calculatorService.clearCurrent_value();
                }
                else{
                    System.out.println("Not a valid operation!");
                }
                System.out.println("\nCurrent value: " + calculatorService.getCurrent_value() + "\n");
                System.out.println("Enter the desired operation: ");
                operation = scanner.nextLine();
            }
            catch (Exception e){
                System.out.println(e.getMessage());
                scanner.nextLine();
                System.out.println("\nEnter the desired operation: ");
                operation = scanner.nextLine();
            }
        }
        System.out.println("Execution stopped.");
    }
}
