package com.example;

public class BasicCalculator implements ICalculator{
    private double current_value;

    public BasicCalculator(){
        this.current_value = 0;
    }

    public void clearCurrent_value(){
        this.current_value = 0;
    }

    public void addVal(int val){
        this.current_value += val;
    }

    public void subtractVal(int val){
        this.current_value -= val;
    }

    public void maxVal(int val) { this.current_value = Math.max(this.current_value, val); }

    public void minVal(int val) { this.current_value = Math.min(this.current_value, val); }

    public void divideVal(int val) throws Exception {
        if(val == 0){
            throw new Exception("Division by zero!");
        }
        this.current_value /= val;
    }

    public void sqrtVal(){
        this.current_value = Math.sqrt(this.current_value);
    }

    public void multiplyVal(int val){
        this.current_value *= val;
    }

    //getter
    public double getCurrent_value() {
        return current_value;
    }

    //setter
    public void setCurrent_value(int current_value) {
        this.current_value = current_value;
    }
}
