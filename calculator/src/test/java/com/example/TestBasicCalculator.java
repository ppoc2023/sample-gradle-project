package com.example;

import org.hamcrest.MatcherAssert;
import static org.hamcrest.Matchers.is;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class TestBasicCalculator {
    public BasicCalculator basicCalculator;

    @BeforeEach
    public void setup(){
        this.basicCalculator = new BasicCalculator();
    }

    @Test
    public void initialValueOfTheCalculatorShouldBeZero(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));
    }

    @Test
    public void addingNumbersShouldIncreaseTheCurrentValue(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        //add 20
        this.basicCalculator.addVal(20);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(20.0)));

        //add 100
        this.basicCalculator.addVal(100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(120.0)));
    }

    @Test
    public void subtractingNumbersShouldDecreaseOrIncreaseTheCurrentValue(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        //subtract 20
        this.basicCalculator.subtractVal(20);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(-20.0)));

        //subtract -20
        this.basicCalculator.subtractVal(-20);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));
    }

    @Test
    public void divisionShouldChangeTheSignOfTheCurrentValue(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        //add 100
        this.basicCalculator.addVal(100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(100.0)));

        //divide with -1
        try{
            this.basicCalculator.divideVal(-1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(-100.0)));

        //divide with -1
        try{
            this.basicCalculator.divideVal(-1);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(100.0)));
    }

    @Test
    public void multiplyingNumbersShouldChangeTheSignOfTheCurrentValue(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        //add 100
        this.basicCalculator.addVal(100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(100.0)));

        //multiply with -1
        this.basicCalculator.multiplyVal(-1);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(-100.0)));

        //multiply with -1
        this.basicCalculator.multiplyVal(-1);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(100.0)));
    }

    @Test
    public void testSqrtOperation(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        //add 100
        this.basicCalculator.addVal(100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(100.0)));

        //sqrt
        this.basicCalculator.sqrtVal();
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(10.0)));
    }

    @Test
    public void testMinOperation(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        this.basicCalculator.minVal(100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        this.basicCalculator.minVal(-100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(-100.0)));
    }

    @Test
    public void testMaxOperation(){
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        this.basicCalculator.maxVal(-100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(0.0)));

        this.basicCalculator.maxVal(100);
        MatcherAssert.assertThat(this.basicCalculator.getCurrent_value(), is(Matchers.equalTo(100.0)));
    }
}
