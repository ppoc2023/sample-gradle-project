import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

public class Tracker {
    private Map<String, Integer> peerMap = new HashMap<>();

    public void start(int port) throws IOException {
        ServerSocket serverSocket = new ServerSocket(port);
        System.out.println("Tracker started. Waiting for connections...");

        while (true) {
            Socket clientSocket = serverSocket.accept();
            System.out.println("New connection accepted: " + clientSocket);

            BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter writer = new PrintWriter(clientSocket.getOutputStream(), true);
            String command = reader.readLine();

            if (command.startsWith("REGISTER")) {
                handleRegistration(command);
            } else if (command.startsWith("UNREGISTER")) {
                handleUnregistration(command);
            } else if (command.equals("LIST")) {
                providePeerList(writer);
            }

            clientSocket.close();
        }
    }

    private void handleUnregistration(String command){
        String[] parts = command.split(" ");
        String peerName = parts[1];
        peerMap.remove(peerName);
    }

    private void handleRegistration(String command) {
        String[] parts = command.split(" ");
        String peerName = parts[1];
        int peerPort = Integer.parseInt(parts[2]);

        peerMap.put(peerName, peerPort);
    }

    private void providePeerList(PrintWriter writer) {
        writer.println(String.join(",", peerMap.entrySet().stream()
                .map(entry -> entry.getKey() + ":" + entry.getValue())
                .toArray(String[]::new)));
    }

    public static void main(String[] args) throws IOException {
        Tracker tracker = new Tracker();
        tracker.start(9090);
    }
}