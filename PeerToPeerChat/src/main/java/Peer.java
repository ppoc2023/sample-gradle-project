import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


public class Peer {
    private String name;
    private ServerSocket serverSocket;
    private Socket peerSocket;
    private volatile BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    private volatile PrintWriter writer = new PrintWriter(System.out);
    private volatile boolean currentlyConnected = false;

    private volatile boolean running = true;

    private static final int NTHREADS = 10;
    private static final ExecutorService exec =
            Executors.newFixedThreadPool(NTHREADS);

    public Peer(String name, int port) throws IOException {
        this.name = name;
        this.serverSocket = new ServerSocket(port);
    }

    public void connectToTracker(String trackerAddress, int trackerPort) throws IOException {
        Socket trackerSocket = new Socket(trackerAddress, trackerPort);
        PrintWriter trackerWriter = new PrintWriter(trackerSocket.getOutputStream(), true);
        trackerWriter.println("REGISTER " + name + " " + serverSocket.getLocalPort());
        trackerSocket.close();
    }

    public void unregisterFromTracker(String trackerAddress, int trackerPort) throws IOException {
        Socket trackerSocket = new Socket(trackerAddress, trackerPort);
        PrintWriter trackerWriter = new PrintWriter(trackerSocket.getOutputStream(), true);
        trackerWriter.println("UNREGISTER " + name);
        trackerSocket.close();
    }

    public boolean connectToPeerFromTracker(String trackerAddress, int trackerPort, String peerName) throws IOException {
        boolean connectionStatus = false;
        if(!currentlyConnected){
            boolean foundPeer = false;
            Socket trackerSocket = new Socket(trackerAddress, trackerPort);
            BufferedReader trackerReader = new BufferedReader(new InputStreamReader(trackerSocket.getInputStream()));
            PrintWriter trackerWriter = new PrintWriter(trackerSocket.getOutputStream(), true);
            trackerWriter.println("LIST");
            String peerList = trackerReader.readLine();
            String[] peers = peerList.split(",");
            for (String peer : peers) {
                String[] parts = peer.split(":");
                String pName = parts[0];
                int peerPort = Integer.parseInt(parts[1]);
                if(pName.compareTo(peerName) == 0 && !currentlyConnected){
                    foundPeer = true;
                    currentlyConnected = true;
                    connectToPeer(peerName, "localhost", peerPort);
                    connectionStatus = true;
                }
            }
            if(!foundPeer){
                System.out.println("Peer with name " + peerName + " wasn't found\n");
            }
            trackerSocket.close();
        }
        else{
            System.out.println("You are already connected to a peer!\n");
        }
        return connectionStatus;
    }

    public void connectToPeer(String peerName, String ipAddress, int port) throws IOException {
        peerSocket = new Socket(ipAddress, port);
        initializeStreams(peerSocket);
    }

    public void closeConnectionWithPeer() throws IOException {
        if (peerSocket != null) {
            peerSocket.close();
            currentlyConnected = false;
        }
    }

    public void acceptIncomingConnection() throws IOException {
        System.out.println("Listening for new connections...\n");
        if(!currentlyConnected){
            peerSocket = serverSocket.accept();
            this.currentlyConnected = true;
            initializeStreams(peerSocket);
        }
    }

    private void initializeStreams(Socket socket) throws IOException {
        this.reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.writer = new PrintWriter(socket.getOutputStream(), true);
        writer.println(new Message(name, "!ack").toJson());
        System.out.println("Sent !ack message to peer.");
    }

    public void sendMessage(Message message) {
        writer.println(message.toJson());
    }

    public Message receiveMessage() throws IOException {
        String json = reader.readLine();
        return Message.fromJson(json);
    }

    public void close() throws IOException {
        synchronized (this){
            if (peerSocket != null) {
                peerSocket.close();
            }
            serverSocket.close();
            currentlyConnected = false;
            running = false;
        }
    }

    public void handleConsoleInput(BufferedReader reader) {
        while(running){
            System.out.print("~" + name + ": ");
            try {
                String inputMessage = reader.readLine();
                if((inputMessage.contains("!hello") && !currentlyConnected) || inputMessage.contains("!bye")){
                    processConsoleMessage(inputMessage);
                } else if (inputMessage.contains("!hello") && currentlyConnected) {
                    System.out.println("You are already connected!\n");
                } else{
                    sendMessage(new Message(name, inputMessage));
                }
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    private void processConsoleMessage(String message) throws IOException {
        if (message.startsWith("!hello")) {
            String name = message.split(" ")[1];
            boolean connectionStatus = connectToPeerFromTracker("localhost", 9090, name);
            if(connectionStatus){
                writer.println(new Message(this.name, "!hello").toJson());
                String ackMessage = Message.fromJson(reader.readLine()).getContent();
                if(ackMessage.compareTo("!ack") == 0){
                    System.out.println("Connection established with " + name);
                    currentlyConnected = true;
                    Runnable handleListeningToMessages = this::listenForNewMessages;
                    exec.execute(handleListeningToMessages);
                }
                else{
                    closeConnectionWithPeer();
                    writer.println(new Message(this.name, "!bye").toJson());
                    System.out.println("Connection failed. Did not receive ack message.");
                }
            }
        } else if (message.equals("!bye")) {
            writer.println(new Message(this.name, "!bye").toJson());
            closeConnectionWithPeer();
            System.out.println("Closed connection with peer.");
            System.out.println("Currently connected status: " + currentlyConnected);
        } else if (message.equals("!byebye")) {
            writer.println(new Message(this.name, "!byebye").toJson());
            unregisterFromTracker("localhost", 9090);
            exec.shutdown();
            closeConnectionWithPeer();
            close();
            System.out.println("Disconnected you from the network.");
            System.exit(0);
        }
    }

    public void handleIncomingConnections() {
        try {
            while(running){
                synchronized (Peer.class){
                    if(!currentlyConnected){
                        acceptIncomingConnection();
                    }
                    else{
                        listenForNewMessages();
                    }
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public void listenForNewMessages() {
        try {
            while(currentlyConnected){
                if(reader.ready()){
                    Message message = receiveMessage();
                    if(message.getContent().contains("!hello")){
                        connectToPeerFromTracker("localhost", 9090, message.getSender());
                        System.out.println("Peer connected.");
                    }
                    else if(message.getContent().contains("!bye")){
                        closeConnectionWithPeer();
                        System.out.println("Peer disconnected.");
                        System.out.println("Currently connected status: " + currentlyConnected);
                    }
                    else {
                        System.out.println("\n" + "~" + message.getSender() + ": " + message.getContent());
                        System.out.print("~" + this.name + ": ");
                    }

                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) throws IOException, InterruptedException {
        InputStreamReader inputStreamReader = new InputStreamReader(System.in);
        BufferedReader reader = new BufferedReader(inputStreamReader);
        System.out.print("Please enter your name: ");
        String name = reader.readLine();
        System.out.print("Please enter a port: ");
        int port = Integer.parseInt(reader.readLine());
        Peer peer = new Peer(name, port);
        Runnable connectToTrackerTask = () -> {
            try {
                peer.connectToTracker("localhost", 9090);
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        };

        exec.execute(connectToTrackerTask);

        Runnable handleConsoleInputTask = () -> {
            peer.handleConsoleInput(reader);
        };

        exec.execute(handleConsoleInputTask);

        Runnable handleIncomingConnectionsTask = peer::handleIncomingConnections;
        exec.execute(handleIncomingConnectionsTask);
    }
}