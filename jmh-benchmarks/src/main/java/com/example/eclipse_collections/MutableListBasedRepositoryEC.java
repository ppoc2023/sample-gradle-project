package com.example.eclipse_collections;

import com.example.InMemoryRepository;
import org.eclipse.collections.api.list.MutableList;
import org.eclipse.collections.impl.list.mutable.FastList;

import java.util.Collection;

public class MutableListBasedRepositoryEC<T> implements InMemoryRepository<T> {

    private MutableList<T> mutableList = FastList.newList();

    @Override
    public void add(T obj) {
        this.mutableList.add(obj);
    }

    @Override
    public void addAll(Collection<T> objects) {
        this.mutableList.addAll(objects);
    }

    @Override
    public boolean contains(T obj) {
        return this.mutableList.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.mutableList.remove(obj);
    }

    public void clear() {
        this.mutableList.clear();
    }
}
