package com.example.fast_util;

import com.example.InMemoryRepository;
import it.unimi.dsi.fastutil.ints.IntArrayList;

import java.util.Collection;

public class IntArrayListBasedRepository implements InMemoryRepository<Integer> {
    private IntArrayList intList = new IntArrayList();

    @Override
    public void add(Integer obj) {
        intList.add(obj.intValue());
    }

    @Override
    public void addAll(Collection<Integer> objects) {
        this.intList.addAll(objects);
    }

    @Override
    public boolean contains(Integer obj) {
        return intList.contains(obj.intValue());
    }

    @Override
    public void remove(Integer obj) {
        intList.rem(obj);
    }

    public void clear(){
        this.intList.clear();
    }
}
