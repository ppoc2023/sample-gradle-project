package com.example.fast_util;

import com.example.InMemoryRepository;
import it.unimi.dsi.fastutil.longs.LongOpenHashSet;

import java.util.Collection;

public class LongOpenHashSetBasedRepository implements InMemoryRepository<Long> {
    private LongOpenHashSet longSet = new LongOpenHashSet();

    @Override
    public void add(Long obj) {
        longSet.add(obj.longValue());
    }

    @Override
    public void addAll(Collection<Long> objects) {
        this.longSet.addAll(objects);
    }

    @Override
    public boolean contains(Long obj) {
        return longSet.contains(obj.longValue());
    }

    @Override
    public void remove(Long obj) {
        longSet.remove(obj.longValue());
    }

    @Override
    public void clear() {
        this.longSet.clear();
    }
}
