package com.example.fast_util;

import com.example.InMemoryRepository;
import org.eclipse.collections.impl.map.mutable.primitive.IntIntHashMap;

import java.util.Collection;

public class IntIntHashMapBasedRepository implements InMemoryRepository<Integer> {

    private IntIntHashMap items = new IntIntHashMap();

    @Override
    public void add(Integer obj) {
        items.put(obj, obj);
    }

    @Override
    public void addAll(Collection<Integer> objects) {
        for(Integer entry: objects){
            this.items.put(entry, entry);
        }
    }

    @Override
    public boolean contains(Integer obj) {
        return items.contains(obj);
    }

    @Override
    public void remove(Integer obj) {
        items.remove(obj);
    }

    public void clear(){
        this.items.clear();
    }
}
