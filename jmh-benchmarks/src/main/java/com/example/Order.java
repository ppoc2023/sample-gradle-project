package com.example;

import java.util.Objects;

public class Order implements Comparable<Order>{
    private int id;
    private int price;
    private int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    public boolean equals(Object obj){
        if(this == obj)
            return true;

        if(obj == null || obj.getClass()!= this.getClass())
            return false;

        Order order = (Order) obj;

        return (this.id == order.id && this.price == order.price && this.quantity == order.quantity);
    }

    public int hashCode(){
        return this.id;
    }


    public String toString(){
        return "Order(" + this.id + ", " + this.price + ", " + this.quantity + ")";
    }

    public int compareTo(Order o){
        int valueFirst = this.price * this.quantity;
        int valueSecond = o.price * o.quantity;
        return Integer.compare(valueFirst, valueSecond);
    }

    public int getId() {
        return id;
    }

    public int getPrice() {
        return price;
    }

    public int getQuantity() {
        return quantity;
    }
}
