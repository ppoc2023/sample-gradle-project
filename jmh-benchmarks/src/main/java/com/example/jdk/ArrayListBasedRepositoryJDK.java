package com.example.jdk;

import com.example.InMemoryRepository;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ArrayListBasedRepositoryJDK<T> implements InMemoryRepository<T> {

    private List<T> list;

    public ArrayListBasedRepositoryJDK(List<T> list) {
        this.list = list;
    }

    public ArrayListBasedRepositoryJDK() {
        this.list = new ArrayList<>();
    }

    @Override
    public void add(T obj) {
        list.add(obj);
    }

    @Override
    public void addAll(Collection<T> objects) {
        this.list.addAll(objects);
    }

    @Override
    public boolean contains(T obj) {
        return this.list.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.list.remove(obj);
    }

    @Override
    public void clear() {
        this.list.clear();
    }
}
