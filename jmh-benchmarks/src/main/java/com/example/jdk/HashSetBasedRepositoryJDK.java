package com.example.jdk;

import com.example.InMemoryRepository;

import java.util.Collection;
import java.util.HashSet;

public class HashSetBasedRepositoryJDK<T> implements InMemoryRepository<T> {

    private HashSet<T> hashSet;

    public HashSetBasedRepositoryJDK(HashSet<T> hashSet) {
        this.hashSet = hashSet;
    }

    public HashSetBasedRepositoryJDK() {
        this.hashSet = new HashSet<>();
    }

    @Override
    public void add(T obj) {
        hashSet.add(obj);
    }

    @Override
    public void addAll(Collection<T> objects) {
        this.hashSet.addAll(objects);
    }

    @Override
    public boolean contains(T obj) {
        return this.hashSet.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.hashSet.remove(obj);
    }

    @Override
    public void clear() {
        this.hashSet.clear();
    }
}
