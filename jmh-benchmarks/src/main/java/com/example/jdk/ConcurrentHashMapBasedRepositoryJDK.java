package com.example.jdk;

import com.example.InMemoryRepository;
import com.example.Order;

import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepositoryJDK implements InMemoryRepository<Order> {

    private ConcurrentHashMap<Integer, Order> concurrentHashMap = new ConcurrentHashMap<>();

    @Override
    public void add(Order obj) {
        concurrentHashMap.put(obj.getId(), obj);
    }

    @Override
    public void addAll(Collection<Order> objects) {
        for(Order o: objects){
            this.concurrentHashMap.put(o.getId(), o);
        }
    }

    @Override
    public boolean contains(Order obj) {
        return concurrentHashMap.containsValue(obj);
    }

    @Override
    public void remove(Order obj) {
        concurrentHashMap.remove(obj.getId());
    }

    @Override
    public void clear() {
        this.concurrentHashMap.clear();
    }


}
