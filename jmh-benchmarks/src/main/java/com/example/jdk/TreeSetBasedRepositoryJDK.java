package com.example.jdk;

import com.example.InMemoryRepository;

import java.util.Collection;
import java.util.TreeSet;

public class TreeSetBasedRepositoryJDK<T> implements InMemoryRepository<T> {

    private TreeSet<T> tTreeSet;

    public TreeSetBasedRepositoryJDK(TreeSet<T> tTreeSet) {
        this.tTreeSet = tTreeSet;
    }

    public TreeSetBasedRepositoryJDK() {
        this.tTreeSet = new TreeSet<>();
    }

    @Override
    public void add(T obj) {
        this.tTreeSet.add(obj);
    }

    @Override
    public void addAll(Collection<T> objects) {
        this.tTreeSet.addAll(objects);
    }

    @Override
    public boolean contains(T obj) {
        return this.tTreeSet.contains(obj);
    }

    @Override
    public void remove(T obj) {
        this.tTreeSet.remove(obj);
    }

    @Override
    public void clear() {
        this.tTreeSet.clear();
    }
}
