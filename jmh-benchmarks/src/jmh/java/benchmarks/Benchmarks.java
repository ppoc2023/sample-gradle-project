package benchmarks;

import com.example.Order;
import com.example.eclipse_collections.MutableListBasedRepositoryEC;
import com.example.fast_util.IntArrayListBasedRepository;
import com.example.fast_util.IntIntHashMapBasedRepository;
import com.example.fast_util.LongOpenHashSetBasedRepository;
import com.example.jdk.ArrayListBasedRepositoryJDK;
import com.example.jdk.ConcurrentHashMapBasedRepositoryJDK;
import com.example.jdk.HashSetBasedRepositoryJDK;
import com.example.jdk.TreeSetBasedRepositoryJDK;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.Arrays;
import java.util.Collection;

public class Benchmarks {
    @State(Scope.Benchmark)
    public static class MutableListExecutionPlan {
        public MutableListBasedRepositoryEC<Order> mutableListBasedRepositoryEC = new MutableListBasedRepositoryEC<>();
        public Collection<Order> orders;

        @Setup(Level.Invocation)
        public void setUp(){
            mutableListBasedRepositoryEC.clear();
            orders = Arrays.asList(new Order(1, 30, 5),
                    new Order(2, 40, 10), new Order(3, 50, 2));
        }
    }

    @State(Scope.Benchmark)
    public static class IntArrayListExecutionPlan {
        public IntArrayListBasedRepository intArrayListBasedRepository = new IntArrayListBasedRepository();
        public Collection<Integer> integers;

        @Setup(Level.Invocation)
        public void setUp(){
            intArrayListBasedRepository.clear();
            integers = Arrays.asList(122, 3, 45, 22, 44, 2);
        }
    }

    @State(Scope.Benchmark)
    public static class IntIntHashMapExecutionPlan {
        public IntIntHashMapBasedRepository intIntHashMapBasedRepository = new IntIntHashMapBasedRepository();
        public Collection<Integer> integers;

        @Setup(Level.Invocation)
        public void setUp(){
            intIntHashMapBasedRepository.clear();
            integers = Arrays.asList(122, 3, 45, 22, 44, 2);
        }
    }

    @State(Scope.Benchmark)
    public static class LongOpenHashSetExecutionPlan{
        public LongOpenHashSetBasedRepository longOpenHashSetBasedRepository = new LongOpenHashSetBasedRepository();
        public Collection<Long> longs;

        @Setup(Level.Invocation)
        public void setUp(){
            longOpenHashSetBasedRepository.clear();
            longs = Arrays.asList(122L, 3L, 45L, 22L, 44L, 2L);
        }
    }

    @State(Scope.Benchmark)
    public static class ArrayListExecutionPlan {
        public ArrayListBasedRepositoryJDK<Order> arrayListBasedRepositoryJDK = new ArrayListBasedRepositoryJDK<>();
        public Collection<Order> orders;

        @Setup(Level.Invocation)
        public void setUp(){
            arrayListBasedRepositoryJDK.clear();
            orders = Arrays.asList(new Order(1, 30, 5),
                    new Order(2, 40, 10), new Order(3, 50, 2));
        }
    }

    @State(Scope.Benchmark)
    public static class ConcurrentHashMapExecutionPlan{
        public ConcurrentHashMapBasedRepositoryJDK concurrentHashMapBasedRepositoryJDK = new ConcurrentHashMapBasedRepositoryJDK();
        public Collection<Order> orders;

        @Setup(Level.Invocation)
        public void setUp(){
            concurrentHashMapBasedRepositoryJDK.clear();
            orders = Arrays.asList(new Order(1, 30, 5),
                    new Order(2, 40, 10), new Order(3, 50, 2));
        }
    }

    @State(Scope.Benchmark)
    public static class HashSetExecutionPlan{
        public HashSetBasedRepositoryJDK<Order> hashSetBasedRepositoryJDK = new HashSetBasedRepositoryJDK<>();
        public Collection<Order> orders;

        @Setup(Level.Invocation)
        public void setUp(){
            hashSetBasedRepositoryJDK.clear();
            orders = Arrays.asList(new Order(1, 30, 5),
                    new Order(2, 40, 10), new Order(3, 50, 2));
        }
    }

    @State(Scope.Benchmark)
    public static class TreeSetExecutionPlan{
        public TreeSetBasedRepositoryJDK<Order> treeSetBasedRepositoryJDK = new TreeSetBasedRepositoryJDK<>();
        public Collection<Order> orders;

        @Setup(Level.Invocation)
        public void setUp(){
            treeSetBasedRepositoryJDK.clear();
            orders = Arrays.asList(new Order(1, 30, 5),
                    new Order(2, 40, 10), new Order(3, 50, 2));
        }
    }

    @Benchmark
    public void mutableListAddBenchmark(MutableListExecutionPlan executionPlan){
        executionPlan.mutableListBasedRepositoryEC.addAll(executionPlan.orders);
    }

    @Benchmark
    public void mutableListContainsBenchmark(MutableListExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.mutableListBasedRepositoryEC.add(order);
        executionPlan.mutableListBasedRepositoryEC.contains(order);
        consumer.consume(order);
    }

    @Benchmark
    public void mutableListRemoveBenchmark(MutableListExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.mutableListBasedRepositoryEC.add(order);
        executionPlan.mutableListBasedRepositoryEC.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void intArrayListAddBenchmark(IntArrayListExecutionPlan executionPlan){
        executionPlan.intArrayListBasedRepository.addAll(executionPlan.integers);

    }

    @Benchmark
    public void intArrayListContainsBenchmark(IntArrayListExecutionPlan executionPlan, Blackhole consumer){
        Integer a = 10;
        executionPlan.intArrayListBasedRepository.add(a);
        executionPlan.intArrayListBasedRepository.contains(a);
        consumer.consume(a);
    }

    @Benchmark
    public void intArrayListRemoveBenchmark(IntArrayListExecutionPlan executionPlan, Blackhole consumer){
        Integer a = 10;
        executionPlan.intArrayListBasedRepository.add(a);
        executionPlan.intArrayListBasedRepository.remove(a);
        consumer.consume(a);
    }

    @Benchmark
    public void intIntHashMapAddBenchmark(IntIntHashMapExecutionPlan executionPlan){
        executionPlan.intIntHashMapBasedRepository.addAll(executionPlan.integers);
    }

    @Benchmark
    public void intIntHashMapContainsBenchmark(IntIntHashMapExecutionPlan executionPlan, Blackhole consumer){
        Integer a = 10;
        executionPlan.intIntHashMapBasedRepository.add(a);
        executionPlan.intIntHashMapBasedRepository.contains(a);
        consumer.consume(a);
    }

    @Benchmark
    public void intIntHashMapRemoveBenchmark(IntIntHashMapExecutionPlan executionPlan, Blackhole consumer){
        Integer a = 10;
        executionPlan.intIntHashMapBasedRepository.add(a);
        executionPlan.intIntHashMapBasedRepository.remove(a);
        consumer.consume(a);
    }

    @Benchmark
    public void LongOpenHashSetAddBenchmark(LongOpenHashSetExecutionPlan executionPlan){
        executionPlan.longOpenHashSetBasedRepository.addAll(executionPlan.longs);
    }

    @Benchmark
    public void LongOpenHashSetContainsBenchmark(LongOpenHashSetExecutionPlan executionPlan, Blackhole consumer){
        Long a = 10L;
        executionPlan.longOpenHashSetBasedRepository.add(a);
        executionPlan.longOpenHashSetBasedRepository.contains(a);
        consumer.consume(a);
    }

    @Benchmark
    public void LongOpenHashSetRemoveBenchmark(LongOpenHashSetExecutionPlan executionPlan, Blackhole consumer){
        Long a = 10L;
        executionPlan.longOpenHashSetBasedRepository.add(a);
        executionPlan.longOpenHashSetBasedRepository.remove(a);
        consumer.consume(a);
    }

    @Benchmark
    public void ArrayListAddBenchmark(ArrayListExecutionPlan executionPlan){
        executionPlan.arrayListBasedRepositoryJDK.addAll(executionPlan.orders);
    }

    @Benchmark
    public void ArrayListContainsBenchmark(ArrayListExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.arrayListBasedRepositoryJDK.add(order);
        executionPlan.arrayListBasedRepositoryJDK.contains(order);
        consumer.consume(order);
    }

    @Benchmark
    public void ArrayListRemoveBenchmark(ArrayListExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.arrayListBasedRepositoryJDK.add(order);
        executionPlan.arrayListBasedRepositoryJDK.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void ConcurrentHashMapAddBenchmark(ConcurrentHashMapExecutionPlan executionPlan){
        executionPlan.concurrentHashMapBasedRepositoryJDK.addAll(executionPlan.orders);
    }

    @Benchmark
    public void ConcurrentHashMapContainsBenchmark(ConcurrentHashMapExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.concurrentHashMapBasedRepositoryJDK.add(order);
        executionPlan.concurrentHashMapBasedRepositoryJDK.contains(order);
        consumer.consume(order);
    }

    @Benchmark
    public void ConcurrentHashMapRemoveBenchmark(ConcurrentHashMapExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.concurrentHashMapBasedRepositoryJDK.add(order);
        executionPlan.concurrentHashMapBasedRepositoryJDK.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void HashSetAddBenchmark(HashSetExecutionPlan executionPlan){
        executionPlan.hashSetBasedRepositoryJDK.addAll(executionPlan.orders);
    }

    @Benchmark
    public void HashSetContainsBenchmark(HashSetExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.hashSetBasedRepositoryJDK.add(order);
        executionPlan.hashSetBasedRepositoryJDK.contains(order);
        consumer.consume(order);
    }

    @Benchmark
    public void HashSetRemoveBenchmark(HashSetExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.hashSetBasedRepositoryJDK.add(order);
        executionPlan.hashSetBasedRepositoryJDK.remove(order);
        consumer.consume(order);
    }

    @Benchmark
    public void TreeSetAddBenchmark(TreeSetExecutionPlan executionPlan){
        executionPlan.treeSetBasedRepositoryJDK.addAll(executionPlan.orders);
    }

    @Benchmark
    public void TreeSetContainsBenchmark(TreeSetExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.treeSetBasedRepositoryJDK.add(order);
        executionPlan.treeSetBasedRepositoryJDK.contains(order);
        consumer.consume(order);
    }

    @Benchmark
    public void TreeSetRemoveBenchmark(TreeSetExecutionPlan executionPlan, Blackhole consumer){
        Order order = new Order(1, 2, 3);
        executionPlan.treeSetBasedRepositoryJDK.add(order);
        executionPlan.treeSetBasedRepositoryJDK.remove(order);
        consumer.consume(order);
    }

    public static void main(String[] args) throws RunnerException {
        Options opt = new OptionsBuilder()
                .include(Benchmarks.class.getSimpleName())
                .forks(1)
                .build();
        new Runner(opt).run();
    }
}
